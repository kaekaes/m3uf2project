package PipoLeyenda;

import java.awt.Font;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;

import Core.Field;
import Core.SWindow;
import Core.Sprite;
import Core.Window;
import PipoLeyenda.SpriteDeriveds.Button;
import PipoLeyenda.SpriteDeriveds.GuiSprite;
import PipoLeyenda.SpriteDeriveds.Pipo;
import PipoLeyenda.SpriteDeriveds.Shop;
import PipoLeyenda.SpriteDeriveds.ShopItem;
import PipoLeyenda.SpriteDeriveds.Text;

/**
 * This is the main, here is the main menu, level selection and the game window
 * with it's field
 * 
 * @author Lucas
 * @see <a href=
 *      "https://docs.google.com/document/d/1aQiXGBi3RYl6hInRLEhXsV-sxPrw79c-8_gjR6hSx14/edit?usp=sharing">
 *      More info </a>
 */
public class MainGameLoop{

	// {{ ---- STARTERS ----
	public static Field field = new Field();
	public static int wWidth;
	public static int wHeight;
//	public static int wWidth = field.getWidth();
//	public static int wHeight = field.getHeight();
	private static int[] lastMousePosition = { -1, -1 };
	private static ArrayList<Sprite> spritesToDraw = new ArrayList<>();
	private static HashMap<Integer, ButtonStates> levels = new HashMap<>();
	private static boolean playing = false;
	// }} ------------------

	// {{ ---- BACKGROUNDS ----
	private static String[] backgrounds = { "res/BGMM/bg1.jpg", "res/BGMM/bg2.gif", "res/BGMM/bg3.png",
			"res/BGMM/bg4.gif", "res/BGMM/bg5.gif", "res/BGMM/bg6.gif", "res/BGMM/bg7.gif", "res/BGMM/bg8.gif",
			"res/BGMM/bg9.gif", "res/BGMM/bg10.gif", "res/BGMM/bg11.gif", "res/BGMM/bg12.gif", "res/BGMM/bg13.gif" };
	private static int changeBackgroundTime = 3000;
	// }}

	// {{ ---- TEXTS ----
	public static Font normalFont = new Font("Monospaced", Font.BOLD, 50);
	public static Font boldFont = new Font("Monospaced", Font.BOLD, 100);
	public static Font lightFont = new Font("Monospaced", Font.BOLD, 25);
	public static int whiteColor = 0xFFFFFF;
	public static int blackColor = 0x000000;
	// }}

	private static int pantsusCounter = 0;

	/**
	 * First function to be called, it initiates the window and shows the main menu
	 * to play, if that window is closed then shows the level selection menu, then
	 * load that level
	 * 
	 * @throws IOException
	 * 
	 * @see #mainMenu()
	 * @see #chooseLevel()
	 */
	public static void main(String[] args) throws InterruptedException, IOException{
		init();
		boolean loop = true;
		while (loop){
			loadSaveFile();
			loop = mainMenu();
			if(!loop)
				continue;
			spritesToDraw.clear();
			int level = chooseLevel();
			if(level > 0 && level != 0 && level != 10){
				playing = true;
				spritesToDraw.clear();
				SWindow.clear();
				InGame.playLevel(level);
				changeBackground(true); // When returns to the Mainmenu a background is choosed, the next bg is loaded
										// by timertask
				playing = false;
			} else if(level == 10){
				shop();
			}
		}
		SWindow.getWindow().close();
		System.exit(0);
	}

	/**
	 * Sets the window unresizeable, the title, the icon, initializes the hashmap of
	 * levels and starts the counter of random backgrounds
	 */
	private static void init(){
		SWindow.getWindow(field);
		wWidth = SWindow.getWindow().getWidth();
		wHeight = SWindow.getWindow().getHeight();

		SWindow.getWindow().setResizable(false);
		SWindow.getWindow().setTitle("La leyenda de Pipo");
		SWindow.getWindow().setIconImage((new ImageIcon("res/Pipo/dogIdleD.png")).getImage());

		timer.schedule(changeBGtask, 0, changeBackgroundTime);

		levels.put(1, ButtonStates.UNLOCK);
		for (int i = 2; i <= 2; i++){
			levels.put(i, ButtonStates.LOCK);
		}
		for (int i = 3; i <= 9; i++){
			levels.put(i, ButtonStates.COMINGSOON);
		}

	}

	/**
	 * Shows the first main menu with the play button and close Because an engine
	 * error the close button is locked
	 * 
	 * @throws InterruptedException
	 */
	private static boolean mainMenu() throws InterruptedException{
		// {{ PLAY BUTTON
		spritesToDraw.add(new Button("1", wWidth / 2 - 128, (wHeight / 5) * 2 - 32 + (wHeight / 4), wWidth / 2 + 128,
				(wHeight / 5) * 2 + 32 + (wHeight / 4)));

		Text playButtonTextOutline = new Text("playButtonTextOutline", "PLAY", wWidth / 2 - (120 / 2) + 3,
				(wHeight / 5) * 2 - (32 / 2) + 3 + (wHeight / 4), 30);
		playButtonTextOutline.font = normalFont;
		playButtonTextOutline.textColor = blackColor;
		playButtonTextOutline.unscrollable = true;
		spritesToDraw.add(playButtonTextOutline);

		Text playButtonText = new Text("playButtonText", "PLAY", wWidth / 2 - (120 / 2),
				(wHeight / 5) * 2 - (32 / 2) + (wHeight / 4), 30);
		playButtonText.font = normalFont;
		playButtonText.textColor = whiteColor;
		playButtonText.unscrollable = true;
		spritesToDraw.add(playButtonText);
		// }}

		// {{ CLOSE BUTTON
		spritesToDraw.add(new Button("2", wWidth / 2 - 128, (wHeight / 5) * 3 - 32 + (wHeight / 6), wWidth / 2 + 128,
				(wHeight / 5) * 3 + 32 + (wHeight / 6), ButtonStates.UNLOCK));

		Text StopButtonTextOutline = new Text("StopButtonTextOutline", "CLOSE", wWidth / 2 - (120 / 2) + 3 - 15,
				(wHeight / 5) * 3 - (32 / 2) + 3 + (wHeight / 6), 30);
		StopButtonTextOutline.font = normalFont;
		StopButtonTextOutline.textColor = blackColor;
		StopButtonTextOutline.unscrollable = true;
		spritesToDraw.add(StopButtonTextOutline);

		Text StopButtonText = new Text("StopButtonText", "CLOSE", wWidth / 2 - (120 / 2) - 15,
				(wHeight / 5) * 3 - (32 / 2) + (wHeight / 6), 30);
		StopButtonText.font = normalFont;
		StopButtonText.textColor = whiteColor;
		StopButtonText.unscrollable = true;
		spritesToDraw.add(StopButtonText);
		// }}

		// {{TITLE
		spritesToDraw.add(new GuiSprite("title", (wWidth / 6) * 1 + (wWidth / 12), (wHeight / 10) * 1, (wWidth / 6) * 3,
				(wHeight / 10) * 3, "res/Title.png"));

		int toChoose = -1;
		while (toChoose == -1){
			toChoose = checkMouseInt(spritesToDraw);
			field.draw(spritesToDraw);
			Thread.sleep(17);
		}

		if(toChoose == 1)
			return true;
		else
			return false;
	}

	/**
	 * Shows a matrix of buttons, each one is a different level
	 * 
	 * @return - The level of the button that has been pressed
	 */
	private static int chooseLevel() throws InterruptedException{
		spritesToDraw.clear();
		// {{ ---- DRAW ----
		// TODO set this in a for...
		Button returnButton = new Button("0", wWidth / 8 * 0 + 20, wHeight / 8 * 7 - 32, 256, 64, 1);
		Button lvl1 = new Button("1", wWidth / 8 * 1 + wWidth / 8 - 128, wHeight / 8 * 2 + wHeight / 8 - 48, 256, 64,
				levels.get(1), 1);
		Button lvl2 = new Button("2", wWidth / 8 * 3 + wWidth / 8 - 128, wHeight / 8 * 2 + wHeight / 8 - 48, 256, 64,
				levels.get(2), 1);
		Button lvl3 = new Button("3", wWidth / 8 * 5 + wWidth / 8 - 128, wHeight / 8 * 2 + wHeight / 8 - 48, 256, 64,
				levels.get(3), 1);
		Button lvl4 = new Button("4", wWidth / 8 * 1 + wWidth / 8 - 128, wHeight / 8 * 3 + wHeight / 8 - 48, 256, 64,
				levels.get(4), 1);
		Button lvl5 = new Button("5", wWidth / 8 * 3 + wWidth / 8 - 128, wHeight / 8 * 3 + wHeight / 8 - 48, 256, 64,
				levels.get(5), 1);
		Button lvl6 = new Button("6", wWidth / 8 * 5 + wWidth / 8 - 128, wHeight / 8 * 3 + wHeight / 8 - 48, 256, 64,
				levels.get(6), 1);
		Button lvl7 = new Button("7", wWidth / 8 * 1 + wWidth / 8 - 128, wHeight / 8 * 4 + wHeight / 8 - 48, 256, 64,
				levels.get(7), 1);
		Button lvl8 = new Button("8", wWidth / 8 * 3 + wWidth / 8 - 128, wHeight / 8 * 4 + wHeight / 8 - 48, 256, 64,
				levels.get(8), 1);
		Button lvl9 = new Button("9", wWidth / 8 * 5 + wWidth / 8 - 128, wHeight / 8 * 4 + wHeight / 8 - 48, 256, 64,
				levels.get(9), 1);
		Button shopButton = new Button("10", wWidth / 8 * 3 + wWidth / 8 - 128, wHeight / 8 * 5 + wHeight / 8 - 48, 256,
				64, 1);
		spritesToDraw.add(returnButton);
		spritesToDraw.add(lvl1);
		spritesToDraw.add(lvl2);
		spritesToDraw.add(lvl3);
		spritesToDraw.add(lvl4);
		spritesToDraw.add(lvl5);
		spritesToDraw.add(lvl6);
		spritesToDraw.add(lvl7);
		spritesToDraw.add(lvl8);
		spritesToDraw.add(lvl9);
		spritesToDraw.add(shopButton);
		// }}

		ArrayList<Sprite> spritesToDrawCopy = new ArrayList<>();
		spritesToDrawCopy.addAll(spritesToDraw);

		for (Sprite s : spritesToDrawCopy){
			if(s instanceof Button){
				Button b = (Button) s;
				if(b.state == ButtonStates.UNLOCK && b.name != null){
					if(!b.name.equals("0") && !b.name.equals("10")){
						Text levelButtonTextOutline = new Text("levelButtonOut" + b.name, b.name,
								b.x1 + (b.x2 - b.x1) / 2 + 3 - 15, b.y1 + (b.y2 - b.y1) / 2 + 3, 15);
						levelButtonTextOutline.font = normalFont;
						levelButtonTextOutline.textColor = blackColor;
						levelButtonTextOutline.unscrollable = true;
						spritesToDraw.add(levelButtonTextOutline);

						Text levelButtonText = new Text("levelButton" + b.name, b.name, b.x1 + (b.x2 - b.x1) / 2 - 15,
								b.y1 + (b.y2 - b.y1) / 2, 15);
						levelButtonText.font = normalFont;
						levelButtonText.textColor = whiteColor;
						levelButtonText.unscrollable = true;
						spritesToDraw.add(levelButtonText);
					} else{
						if(b.name.equals("0")){
							Text returnButtonTextOutline = new Text("returnButtonOut", "RETURN",
									b.x1 + ((b.x2 - b.x1) / 2 - 60) / 2 + 3, b.y1 + (b.y2 - b.y1) / 2 + 3, 15);
							returnButtonTextOutline.font = normalFont;
							returnButtonTextOutline.textColor = blackColor;
							returnButtonTextOutline.unscrollable = true;
							spritesToDraw.add(returnButtonTextOutline);

							Text returnButtonText = new Text("returnButton", "RETURN",
									b.x1 + ((b.x2 - b.x1) / 2 - 60) / 2, b.y1 + (b.y2 - b.y1) / 2, 15);
							returnButtonText.font = normalFont;
							returnButtonText.textColor = whiteColor;
							returnButtonText.unscrollable = true;
							spritesToDraw.add(returnButtonText);
						} else{
							Text returnButtonTextOutline = new Text("returnButtonOut", "SHOP",
									b.x1 + ((b.x2 - b.x1) / 2 + 15) / 2 + 3, b.y1 + (b.y2 - b.y1) / 2 + 3, 15);
							returnButtonTextOutline.font = normalFont;
							returnButtonTextOutline.textColor = blackColor;
							returnButtonTextOutline.unscrollable = true;
							spritesToDraw.add(returnButtonTextOutline);

							Text returnButtonText = new Text("returnButton", "SHOP",
									b.x1 + ((b.x2 - b.x1) / 2 + 15) / 2, b.y1 + (b.y2 - b.y1) / 2, 15);
							returnButtonText.font = normalFont;
							returnButtonText.textColor = whiteColor;
							returnButtonText.unscrollable = true;
							spritesToDraw.add(returnButtonText);
						}
					}
				}
			}
		}

		int toChoose = -1;
		while (toChoose == -1){
			toChoose = checkMouseInt(spritesToDraw);
			field.draw(spritesToDraw);
			Thread.sleep(17);
		}
		spritesToDraw.clear();
		return toChoose;
	}

	/**
	 * Creates a shop men� where you can buy skins in exchange of panties
	 * 
	 * @return
	 * @throws InterruptedException
	 * @throws IOException
	 */
	private static void shop() throws InterruptedException, IOException{

		// {{ BUTTONS AND SPRITES

		Button returnButton = new Button("0", wWidth / 8 * 0 + 20, wHeight / 8 * 7 - 32, 256, 64, 1);

		if(Shop.getInstance().items.isEmpty()){
			ShopItem item1 = new ShopItem("shopitem1", wWidth / 8 * 1 + wWidth / 8 - 128,
					wHeight / 8 * 1 + wHeight / 8 - 48 - 32, 256, 132, Pipo.pipoImages[0][2], 0);
			item1.bought = true;
			item1.selected = true;
			ShopItem item2 = new ShopItem("shopitem2", wWidth / 8 * 3 + wWidth / 8 - 128,
					wHeight / 8 * 1 + wHeight / 8 - 48 - 32, 256, 132, Pipo.pipoImages[1][2], 5);
			ShopItem item3 = new ShopItem("shopitem3", wWidth / 8 * 5 + wWidth / 8 - 128,
					wHeight / 8 * 1 + wHeight / 8 - 48 - 32, 256, 132, "res/black.png", 10, false);
			ShopItem item4 = new ShopItem("shopitem4", wWidth / 8 * 1 + wWidth / 8 - 128,
					wHeight / 8 * 4 + wHeight / 8 - 48 - 48, 256, 132, "res/black.png", 15, false);
			ShopItem item5 = new ShopItem("shopitem5", wWidth / 8 * 3 + wWidth / 8 - 128,
					wHeight / 8 * 4 + wHeight / 8 - 48 - 48, 256, 132, "res/black.png", 20, false);
			ShopItem item6 = new ShopItem("shopitem6", wWidth / 8 * 5 + wWidth / 8 - 128,
					wHeight / 8 * 4 + wHeight / 8 - 48 - 48, 256, 132, "res/black.png", 25, false);
			Shop.getInstance().addItem(item1);
			Shop.getInstance().addItem(item2);
			Shop.getInstance().addItem(item3);
			Shop.getInstance().addItem(item4);
			Shop.getInstance().addItem(item5);
			Shop.getInstance().addItem(item6);
		}

		Button btn1 = new Button("1", wWidth / 8 * 1 + wWidth / 8 - 128, wHeight / 8 * 3 + wHeight / 8 - 48 - 64, 256,
				64, Shop.getInstance().items.get(0).able(), 1);
		Button btn2 = new Button("2", wWidth / 8 * 3 + wWidth / 8 - 128, wHeight / 8 * 3 + wHeight / 8 - 48 - 64, 256,
				64, Shop.getInstance().items.get(1).able(), 1);
		Button btn3 = new Button("3", wWidth / 8 * 5 + wWidth / 8 - 128, wHeight / 8 * 3 + wHeight / 8 - 48 - 64, 256,
				64, Shop.getInstance().items.get(2).able(), 1);
		Button btn4 = new Button("4", wWidth / 8 * 1 + wWidth / 8 - 128, wHeight / 8 * 6 + wHeight / 8 - 48 - 80, 256,
				64, Shop.getInstance().items.get(3).able(), 1);
		Button btn5 = new Button("5", wWidth / 8 * 3 + wWidth / 8 - 128, wHeight / 8 * 6 + wHeight / 8 - 48 - 80, 256,
				64, Shop.getInstance().items.get(4).able(), 1);
		Button btn6 = new Button("6", wWidth / 8 * 5 + wWidth / 8 - 128, wHeight / 8 * 6 + wHeight / 8 - 48 - 80, 256,
				64, Shop.getInstance().items.get(5).able(), 1);

		Button resetButton = new Button("7", wWidth / 8 * 6 + 20, wHeight / 8 * 7 - 32, 256, 64, 1);

		spritesToDraw.add(returnButton);
		spritesToDraw.addAll(Shop.getInstance().items);
		spritesToDraw.add(btn1);
		spritesToDraw.add(btn2);
		spritesToDraw.add(btn3);
		spritesToDraw.add(btn4);
		spritesToDraw.add(btn5);
		spritesToDraw.add(btn6);
		spritesToDraw.add(resetButton);

		spritesToDraw.add(new Sprite("panties", 25, 25, 85, 85, "res/GUI/pantsus.png"));
		Text pantCountTextOutline = new Text("panties", pantsusCounter + "", 103, 58, 15);
		pantCountTextOutline.font = normalFont;
		pantCountTextOutline.textColor = blackColor;
		pantCountTextOutline.unscrollable = true;
		spritesToDraw.add(pantCountTextOutline);

		Text pantCountText = new Text("panties", pantsusCounter + "", 100, 55, 15);
		pantCountText.font = normalFont;
		pantCountText.textColor = whiteColor;
		pantCountText.unscrollable = true;
		spritesToDraw.add(pantCountText);

		// }}

		// {{ Generates all button's texts

		ArrayList<Sprite> spritesToDrawCopy = new ArrayList<>();
		spritesToDrawCopy.addAll(spritesToDraw);

		for (Sprite s : spritesToDrawCopy){
			if(s instanceof Button){
				Button b = (Button) s;
				if(b.state != ButtonStates.COMINGSOON && b.name != null){
					if(!b.name.equals("0") && !b.name.equals("7")){
						if(b.state == ButtonStates.UNLOCK){
							int price = ((ShopItem) spritesToDraw.get(Integer.parseInt(b.name))).price;
							Text buyButtonTextOutline = new Text("buybutton" + b.name, "BUY " + price,
									b.x1 + (b.x2 - b.x1) / 2 + 3 - 75, b.y1 + (b.y2 - b.y1) / 2 + 3, 15);
							buyButtonTextOutline.font = normalFont;
							buyButtonTextOutline.textColor = blackColor;
							buyButtonTextOutline.unscrollable = true;
							spritesToDraw.add(buyButtonTextOutline);

							Text buyButtonText = new Text("buybutton" + b.name, "BUY " + price,
									b.x1 + (b.x2 - b.x1) / 2 - 75, b.y1 + (b.y2 - b.y1) / 2, 15);
							buyButtonText.font = normalFont;
							buyButtonText.textColor = whiteColor;
							buyButtonText.unscrollable = true;
							spritesToDraw.add(buyButtonText);
						} else if(b.state == ButtonStates.DONE){
							if(Integer.parseInt(b.name) - 1 == Pipo.actualSkin){
								Text buyButtonTextOutline = new Text("buybutton" + b.name, "SELECTED",
										b.x1 + (b.x2 - b.x1) / 2 + 3 - 120, b.y1 + (b.y2 - b.y1) / 2 + 3, 15);
								buyButtonTextOutline.font = normalFont;
								buyButtonTextOutline.textColor = blackColor;
								buyButtonTextOutline.unscrollable = true;
								spritesToDraw.add(buyButtonTextOutline);

								Text buyButtonText = new Text("buybutton" + b.name, "SELECTED",
										b.x1 + (b.x2 - b.x1) / 2 - 120, b.y1 + (b.y2 - b.y1) / 2, 15);
								buyButtonText.font = normalFont;
								buyButtonText.textColor = whiteColor;
								buyButtonText.unscrollable = true;
								spritesToDraw.add(buyButtonText);
							} else{
								Text buyButtonTextOutline = new Text("buybutton" + b.name, "BOUGHT",
										b.x1 + (b.x2 - b.x1) / 2 + 3 - 90, b.y1 + (b.y2 - b.y1) / 2 + 3, 15);
								buyButtonTextOutline.font = normalFont;
								buyButtonTextOutline.textColor = blackColor;
								buyButtonTextOutline.unscrollable = true;
								spritesToDraw.add(buyButtonTextOutline);

								Text buyButtonText = new Text("buybutton" + b.name, "BOUGHT",
										b.x1 + (b.x2 - b.x1) / 2 - 90, b.y1 + (b.y2 - b.y1) / 2, 15);
								buyButtonText.font = normalFont;
								buyButtonText.textColor = whiteColor;
								buyButtonText.unscrollable = true;
								spritesToDraw.add(buyButtonText);
							}
						}
					} else{
						if(b.name.equals("0")){
							Text returnButtonTextOutline = new Text("returnButtonOut", "RETURN",
									b.x1 + ((b.x2 - b.x1) / 2 - 60) / 2 + 3, b.y1 + (b.y2 - b.y1) / 2 + 3, 15);
							returnButtonTextOutline.font = normalFont;
							returnButtonTextOutline.textColor = blackColor;
							returnButtonTextOutline.unscrollable = true;
							spritesToDraw.add(returnButtonTextOutline);

							Text returnButtonText = new Text("returnButton", "RETURN",
									b.x1 + ((b.x2 - b.x1) / 2 - 60) / 2, b.y1 + (b.y2 - b.y1) / 2, 15);
							returnButtonText.font = normalFont;
							returnButtonText.textColor = whiteColor;
							returnButtonText.unscrollable = true;
							spritesToDraw.add(returnButtonText);
						} else if(b.name.equals("7")){
							Text resetTextOutline = new Text("resetButtonOut", "RESET",
									b.x1 + ((b.x2 - b.x1) / 2 - 30) / 2 + 3, b.y1 + (b.y2 - b.y1) / 2 + 3, 15);
							resetTextOutline.font = normalFont;
							resetTextOutline.textColor = blackColor;
							resetTextOutline.unscrollable = true;
							spritesToDraw.add(resetTextOutline);

							Text resetButtonText = new Text("resetButton", "RESET", b.x1 + ((b.x2 - b.x1) / 2 - 30) / 2,
									b.y1 + (b.y2 - b.y1) / 2, 15);
							resetButtonText.font = normalFont;
							resetButtonText.textColor = whiteColor;
							resetButtonText.unscrollable = true;
							spritesToDraw.add(resetButtonText);
						}
					}
				}
			}
		}

		// }}

		boolean keepInShop = true;
		int toChoose = -1;
		while (keepInShop){

			// {{ Main Loop of shop
			toChoose = -1;
			while (toChoose == -1){
				toChoose = checkMouseInt(spritesToDraw);
				field.draw(spritesToDraw);
				Thread.sleep(17);
			}
			// }}

			if(toChoose != 0 && toChoose != 7){
				Button b = (Button) spritesToDraw.get(toChoose + 6);

				// Select an already bought skin as pipo's skin
				if(b.state == ButtonStates.DONE && Pipo.actualSkin != Integer.parseInt(b.name) - 1){
					for (ShopItem s : Shop.getInstance().items){
						s.selected = false;
					}
					Shop.getInstance().items.get(toChoose - 1).selected = true;
					for (Sprite s : spritesToDraw){
						if(s instanceof Text){
							if(s.name.equals("buybutton" + b.name)){
								s.path = "SELECTED";
								s.x1 -= 30;
							} else if(s.name.contains("buybutton")){
								s.path = "BOUGHT";
								s.x1 += 30;
							}
						}
						Pipo.actualSkin = toChoose - 1;
					}
				}
				// Buy a skin if you have the needed pantsus
				else if(b.state == ButtonStates.UNLOCK){
					int price = Shop.getInstance().items.get(toChoose - 1).price;
					if(pantsusCounter - price >= 0){
						pantsusCounter -= price;
						pantCountText.path = pantsusCounter + "";
						pantCountTextOutline.path = pantsusCounter + "";
						Shop.getInstance().items.get(toChoose - 1).bought = true;
						b.state = Shop.getInstance().items.get(toChoose - 1).able();
						b.checkState();
						for (Sprite s : spritesToDraw){
							if(s instanceof Text && s.name.equals("buybutton" + b.name)){
								s.path = "BOUGHT";
								s.x1 -= 15;
							}
						}
					}
				}
			} else if(toChoose == 7){
				ArrayList<Sprite> spritesToDraw = new ArrayList<Sprite>();
				Button btnReset1 = new Button("1", wWidth / 8 * 2 + wWidth / 8 - 128,
						wHeight / 8 * 4 + wHeight / 8 - 48 - 64, 256, 64, 1);
				Button btnReset2 = new Button("2", wWidth / 8 * 4 + wWidth / 8 - 128,
						wHeight / 8 * 4 + wHeight / 8 - 48 - 64, 256, 64, 1);
				spritesToDraw.add(btnReset1);
				spritesToDraw.add(btnReset2);

				// {{ Add texts
				ArrayList<Sprite> spritesToDrawCopy2 = new ArrayList<>();
				spritesToDrawCopy2.addAll(spritesToDraw);

				for (Sprite s : spritesToDrawCopy2){
					Button b = (Button) s;
					String t = "";
					if(b.name.equals("1")) 
						t = "YES";
					else
						t = "NO";
					
					Text resetButtonTextOutline = new Text("resetbutton" + b.name, t,
							b.x1 + (b.x2 - b.x1) / 2 + 3 - 35, b.y1 + (b.y2 - b.y1) / 2 + 3, 15);
					resetButtonTextOutline.font = normalFont;
					resetButtonTextOutline.textColor = blackColor;
					resetButtonTextOutline.unscrollable = true;
					spritesToDraw.add(resetButtonTextOutline);

					Text resetButtonText = new Text("resetbutton" + b.name, t, b.x1 + (b.x2 - b.x1) / 2 - 35,
							b.y1 + (b.y2 - b.y1) / 2, 15);
					resetButtonText.font = normalFont;
					resetButtonText.textColor = whiteColor;
					resetButtonText.unscrollable = true;
					spritesToDraw.add(resetButtonText);
				}

				// }}

				int toChooseReset = -1;
				while (toChooseReset == -1){
					toChooseReset = checkMouseInt(spritesToDraw);
					field.draw(spritesToDraw);
					Thread.sleep(17);
				}

				if(toChooseReset == 1){
					System.out.println("hola");
					try{
						File file = new File("files/save00.save");
						file.delete();
						pantsusCounter = 0;
						Pipo.actualSkin = 0;
						Shop.getInstance().items.clear();
						toChoose = 0;
					} catch (Exception e){
						System.out.println(e);
					}
				}
			}
			keepInShop = toChoose != 0;
		}
		spritesToDraw.clear();

		makeSaveFile();

	}

//{{ BUTTON FUNCTIONALITIES

	/**
	 * This returns a false if a button was pressed
	 * 
	 * @param sprites - All sprites of the field
	 * @return - False if a button is pressed
	 */
	public static boolean checkMouseBool(ArrayList<Sprite> sprites){
		int x = field.getMouseX();
		int y = field.getMouseY();

		for (Sprite s : sprites){
			if(s.collidesWithPoint(x, y) && x != lastMousePosition[0] && y != lastMousePosition[1]
					&& s instanceof Button){
				Button but = (Button) s;
				if(but.state == ButtonStates.UNLOCK || but.state == ButtonStates.DONE){
					System.out.println(x + " : " + y + " | " + s.name);
					lastMousePosition[0] = x;
					lastMousePosition[1] = y;
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * This class returns the name of the button pressed as integer
	 * 
	 * @param sprites - All sprites of the field
	 * @return - Name of the button parsed as int
	 */
	private static int checkMouseInt(ArrayList<Sprite> sprites){
		int x = field.getMouseX();
		int y = field.getMouseY();

		for (Sprite s : sprites){
			if(s.collidesWithPoint(x, y) && x != lastMousePosition[0] && y != lastMousePosition[1]
					&& s instanceof Button){
				Button but = (Button) s;
				if(but.state == ButtonStates.UNLOCK || but.state == ButtonStates.DONE){
					System.out.println(x + " : " + y + " | " + s.name);
					lastMousePosition[0] = x;
					lastMousePosition[1] = y;
					return Integer.parseInt(but.name);
				}
			}
		}
		return -1;
	}
//}}

	/**
	 * Marks a level as cleared and unlocks the next level
	 * 
	 * @param level - The level to set as cleared
	 */
	public static void levelCompleted(int level){
		levels.put(level, ButtonStates.DONE);
		if(levels.get(level + 1) == ButtonStates.LOCK)
			levels.put(level + 1, ButtonStates.UNLOCK);

	}

	/**
	 * Changes the Alexelcapo's backgounds
	 * 
	 * @param skipCheck - If should skip the first check
	 */
	private static void changeBackground(boolean skipCheck){
		if(!playing || skipCheck){
			Random r = new Random();
			field.background = backgrounds[r.nextInt(backgrounds.length)];
		}
	}

	private static void loadSaveFile(){
		try{
			File file = new File("files/save00.save");
			boolean b = file.createNewFile();
			if(b){
				BufferedWriter bw = new BufferedWriter(new FileWriter("files/save00.save"));
				bw.write("pantsus:0");
				bw.newLine();
				bw.write("skin:0");
				bw.newLine();
				bw.write("shop:[]");
				bw.flush();
				bw.close();
			} else{
				BufferedReader br = new BufferedReader(new FileReader(file));
				String line;
				while ((line = br.readLine()) != null){
					if(line.contains("pantsus")){
						String[] l = line.split(":");
						pantsusCounter = Integer.parseInt(l[1]);
					} else if(line.contains("skin")){
						String[] l = line.split(":");
						Pipo.actualSkin = Integer.parseInt(l[1]);
					} else if(line.contains("shop")){
						String[] l = line.split(":");
						String[] l2 = l[1].split(", ");
						if(l2.length > 1){
							ArrayList<ShopItem> items = new ArrayList<ShopItem>();
							for (String s : l2){
								s = s.replace("[", "");
								s = s.replace("]", "");
								String[] sl = s.split(";");
								ShopItem i = new ShopItem(sl[4], Integer.parseInt(sl[5]), Integer.parseInt(sl[6]),
										Integer.parseInt(sl[7]), Integer.parseInt(sl[8]), sl[9]);
								i.price = Integer.parseInt(sl[0]);
								i.bought = Boolean.parseBoolean(sl[1]);
								i.buyable = Boolean.parseBoolean(sl[2]);
								i.selected = Boolean.parseBoolean(sl[3]);
								items.add(i);
							}
							Shop.getInstance().items = items;

						}
//						Shop.getInstance().items = l[1];
					}
				}
			}
		} catch (Exception e){
			System.out.println("Ha muerto el buffer de files en main load " + e);
		}
	}

	/**
	 * Saves the actual pantus in a file
	 * 
	 * @throws IOException
	 */
	private static void makeSaveFile() throws IOException{

		try{
			File file = new File("files/save00.save");
			boolean b = file.createNewFile();
			if(b){
				BufferedWriter bw = new BufferedWriter(new FileWriter("files/save00.save"));
				bw.write("pantsus:0");
				bw.newLine();
				bw.write("skin:0");
				bw.newLine();
				bw.write("shop:[]");
				bw.flush();
				bw.close();
			}
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			int pants = 0;
			while ((line = br.readLine()) != null){
				if(line.contains("pantsus")){
					String[] l = line.split(":");
					pants = Integer.parseInt(l[1]);
				}
			}

			BufferedWriter bw = new BufferedWriter(new FileWriter("files/save00.save"));
			bw.write("pantsus:" + pantsusCounter);
			bw.newLine();
			bw.write("skin:" + Pipo.actualSkin);
			bw.newLine();
			bw.write(Shop.getInstance().toString());
			bw.flush();

			bw.close();
			br.close();

		} catch (Exception e){
			System.out.println("Ha muerto el buffer de files en main save " + e);
		}
	}

	private static Timer timer = new Timer();
	/**
	 * Timer for {@link #changeBackground(boolean)}
	 */
	private static TimerTask changeBGtask = new TimerTask(){
		@Override
		public void run(){
			changeBackground(false);
		}
	};
}
