package PipoLeyenda.SpriteDeriveds;

import java.io.Serializable;
import java.util.ArrayList;

public class Shop implements Serializable{
	
	public ArrayList<ShopItem> items = new ArrayList<ShopItem>();
	
	private static Shop instance = null;
	
	public static Shop getInstance() {
		if(instance == null) {
			instance = new Shop();
		}
		return instance;
	}
	
	public void addItem(ShopItem item) {
		items.add(item);
	}

	@Override
	public String toString(){
		return "shop:"+items.toString();
	}
	
	
}
