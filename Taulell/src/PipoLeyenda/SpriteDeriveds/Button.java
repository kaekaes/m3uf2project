package PipoLeyenda.SpriteDeriveds;

import Core.Sprite;
import PipoLeyenda.ButtonStates;

/**
 * Generates a button They can have the states of LevelStates, with each one
 * changes the sprite
 * 
 * @see PipoLeyenda.ButtonStates
 * @author Lucas
 */
public class Button extends Sprite{

	public ButtonStates state = ButtonStates.UNLOCK;

	/**
	 * Auto-Enabled button
	 * 
	 * @param name - Inner engine name
	 * @param x1   - Top left position horizontally
	 * @param y1   - Top left position vertically
	 * @param x2   - Bottom right position horizontally
	 * @param y2   - Bottom right position vertically
	 * @param path - The source of the sprite
	 */
	public Button(String name, int x1, int y1, int x2, int y2){
		super(name, x1, y1, x2, y2, "");
		checkState();
	}

	/**
	 * Auto-Enabled button
	 * 
	 * @param name   - Inner engine name
	 * @param x1     - Top left position horizontally
	 * @param y1     - Top left position vertically
	 * @param width  - The width of the sprite
	 * @param height - The height of the sprite
	 * @param path   - The source of the sprite
	 * @param scrap  - Nothing, just for making another constructor
	 */
	public Button(String name, int x1, int y1, int width, int height, int scrap){
		super(name, x1, y1, x1 + width, y1 + height, "");
		checkState();
	}

	/**
	 * Button that you can set the initial state
	 * 
	 * @param name     - Inner engine name
	 * @param x1       - Top left position horizontally
	 * @param y1       - Top left position vertically
	 * @param x2       - Bottom right position horizontally
	 * @param y2       - Bottom right position vertically
	 * @param inactive - if it is enabled or not
	 */
	public Button(String name, int x1, int y1, int x2, int y2, ButtonStates status){
		super(name, x1, y1, x2, y2, "");
		this.state = status;
		checkState();
	}

	/**
	 * Button that you can set the initial state
	 * 
	 * @param name     - Inner engine name
	 * @param x1       - Top left position horizontally
	 * @param y1       - Top left position vertically
	 * @param width    - The width of the sprite
	 * @param height   - The height of the sprite
	 * @param inactive - if it is enabled or not
	 */
	public Button(String name, int x1, int y1, int width, int height, ButtonStates status, int scrap){
		super(name, x1, y1, x1 + width, y1 + height, "");
		this.state = status;
		checkState();
	}

	/**
	 * Changes the button sprite depending of its state
	 */
	public void checkState(){
		if(state == ButtonStates.UNLOCK)
			changeImage("res/Buttons/pipoButtonUnlock.png");
		else if(state == ButtonStates.LOCK)
			changeImage("res/Buttons/pipoButtonLock.png");
		else if(state == ButtonStates.DONE)
			changeImage("res/Buttons/pipoButtonDone.png");
		else if(state == ButtonStates.COMINGSOON)
			changeImage("res/Buttons/pipoButtonComingSoon.png");
	}

}