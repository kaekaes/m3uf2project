package PipoLeyenda.SpriteDeriveds;

import Core.Sprite;

/**
 * For Sprites that will be on GUI
 * 
 * @author Lucas
 */
public class GuiSprite extends Sprite{

	/**
	 * Squared Gui sprites
	 * 
	 * @param name - Inner engine name
	 * @param x1   - Top left position horizontally
	 * @param y1   - Top left position vertically
	 * @param size - Both (horizontal and vertical size)
	 * @param path - The source of the sprite
	 */
	public GuiSprite(String name, int x1, int y1, int size, String path){
		super(name, x1, y1, x1 + size, y1 + size, path);
		this.solid = false;
		this.physicBody = false;
		this.terrain = false;
		this.unscrollable = true;
	}

	/**
	 * No squared Gui sprites
	 * 
	 * @param name   - Inner engine name
	 * @param x1     - Top left position horizontally
	 * @param y1     - Top left position vertically
	 * @param width  - The width of the sprite
	 * @param height - The height of the sprite
	 * @param path   - The source of the sprite
	 */
	public GuiSprite(String name, int x1, int y1, int width, int height, String path){
		super(name, x1, y1, x1 + width, y1 + height, path);
		this.solid = false;
		this.physicBody = false;
		this.terrain = false;
		this.unscrollable = true;
	}

}
