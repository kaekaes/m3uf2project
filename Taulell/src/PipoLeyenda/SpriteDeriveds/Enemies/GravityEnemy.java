package PipoLeyenda.SpriteDeriveds.Enemies;

import PipoLeyenda.InGame;
import PipoLeyenda.MainGameLoop;

/**
 * 
 * Is a child of {@link Enemy} but it uses {@link GravityEnemy#gravity()}
 * 
 * @author Lucas
 *
 */
public abstract class GravityEnemy extends Enemy{

	final float GRAVITYFORCE = 0.2f;
	boolean onGround = false;

	/**
	 * 
	 * Constructor for Enemies with gravity
	 * 
	 * @param x1        - Top left position horizontally
	 * @param y1        - Top left position vertically
	 * @param width     - Width of the Enemy
	 * @param height    - Height of the Enemy
	 * 
	 * @param minX      - From this position, the leftmost position that this will
	 *                  move
	 * @param maxX      - From this position, the rightmost position that this will
	 *                  move
	 * @param speed     - The speed of the movement
	 * @param knockback - Knockback values, first is horizontally, second is
	 * @param path      - The source of the sprite
	 */
	public GravityEnemy(int x1, int y1, int width, int height, int minX, int maxX, int speed, int[] knockback,
			String path){
		super(x1, y1, width, height, minX, maxX, speed, knockback, path);
		this.trigger = true;
	}

	/**
	 * Makes the enemy have gravity if it needs
	 */
	public void gravity(){
		if(!this.onGround){
			if(collidesWithPercent(InGame.sueloGeneral) != 0){
				this.onGround = true;
				this.physicBody = false;
			} else{
				this.physicBody = true;
				setConstantForce(0, GRAVITYFORCE);
			}
		} else{
			if(collidesWithPercent(InGame.sueloGeneral) == 0){
				this.onGround = false;
			}
		}
	}

}
