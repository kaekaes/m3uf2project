package PipoLeyenda.SpriteDeriveds;

import PipoLeyenda.SpriteDeriveds.Enemies.Enemy;

/**
 * 
 * Interface of things that can stun {@link Enemy}
 * 
 * @author Lucas
 *
 */
public interface Stunable {

	/**
	 * Implements a way of stunning
	 * @param enemy - Enemy to be stunned
	 */
	void wayForStunning(Enemy enemy);
}
