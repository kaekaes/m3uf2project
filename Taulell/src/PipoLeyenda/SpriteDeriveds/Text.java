package PipoLeyenda.SpriteDeriveds;

import Core.Sprite;

/**
 * Simple class for creating texts
 * 
 * @author Lucas
 *
 */
public class Text extends Sprite{

	/**
	 * Creates a text
	 * 
	 * @param name    - Inner name for the engine
	 * @param text    - Text to be displayed
	 * @param x1      - Top left position horizontally
	 * @param y1      - Top left position vertically
	 * @param spacing - Top spacing
	 */
	public Text(String name, String text, int x1, int y1, int spacing){
		super(name, x1, y1, x1, y1 + spacing, text);
		this.text = true;
		this.solid = false;
		this.physicBody = false;
		this.terrain = false;
	}
}
