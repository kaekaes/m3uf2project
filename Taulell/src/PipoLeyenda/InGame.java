package PipoLeyenda;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import Core.SWindow;
import Core.Sprite;
import PipoLeyenda.SpriteDeriveds.Background;
import PipoLeyenda.SpriteDeriveds.Button;
import PipoLeyenda.SpriteDeriveds.FallEntity;
import PipoLeyenda.SpriteDeriveds.Platform;
import PipoLeyenda.SpriteDeriveds.Shop;
import PipoLeyenda.SpriteDeriveds.GuiSprite;
import PipoLeyenda.SpriteDeriveds.Pipo;
import PipoLeyenda.SpriteDeriveds.Text;
import PipoLeyenda.SpriteDeriveds.Enemies.Enemy;
import PipoLeyenda.SpriteDeriveds.Enemies.GravityEnemy;

/**
 * This class is the game, it loads levels, gets inputs and shows everything on
 * MainGameLoop's window
 * 
 * @author Lucas
 */
public class InGame{

	// {{ GUI
	public static GuiSprite bleachGauge;
	// {{ COUNTERS
	public static int pantsusCounter;
	public static int bleachCounter;
	private static int guiSpriteSize = 80;
	// }}

	// {{ TEXT
	private static Text pantsusCounterTxt;
	private static Text bleachCounterTxt;
	// }}
	// }}

	// {{ SPRITES
	public static Pipo pipo;
	public static Platform sueloGeneral;
	public static Background background;
	public static ArrayList<Sprite> allSprites = new ArrayList<Sprite>();
	public static ArrayList<Platform> allPlatforms = new ArrayList<Platform>();
	public static ArrayList<Enemy> allEnemies = new ArrayList<Enemy>();

	private static String[] bleachGaugeImages = { "res/BleachGauge/BleachGaugeV.png",
			"res/BleachGauge/BleachGaugeR.png", "res/BleachGauge/BleachGaugeY.png",
			"res/BleachGauge/BleachGaugeG.png" };
	// }}

	private static boolean inGame = false;
	public static int actualLevel = 0;

	private static int[] lastMousePosition = { -1, -1 };

	/**
	 * This function is the "main" of the game, it gets the level to load and calls
	 * for its sprites and resources to play the level. Then plays a gameloop, if
	 * the level is over checks if it was a win or not and cleans all the lists,
	 * then returns to the {@link MainGameLoop#main()}
	 * 
	 * @param level - The level to be played
	 * @throws IOException 
	 */
	public static void playLevel(int level) throws InterruptedException, IOException{
		actualLevel = level;
		init();
		loadGameScreen();
		int fps = 0, i = 0, j = 0;
		double actualtime = System.currentTimeMillis();
		while (inGame){
			// {{ ---- FPS ----
			i++;
			if(actualtime + 1000 <= System.currentTimeMillis()){
				j++;
				fps += i;
				i = 0;
				actualtime = System.currentTimeMillis();
//				System.out.println("FPS: " + (fps / j));
			}
			// }}

			inputs();
			teleportAux();
			MainGameLoop.field.draw(spritesFrustumCulling());

//			Thread.sleep(20);
			TimeUnit.MILLISECONDS.sleep(33);
		}
		boolean win = haveWon();
		if(win)
			endWinning();
		else
			endLoosing(); // TODO replay button

		cleanUP();
	}

	private static void teleportAux(){
		int x = MainGameLoop.field.getMouseX();
		int y = MainGameLoop.field.getMouseY();
		if(x != lastMousePosition[0] && y != lastMousePosition[1]){
			pipo.x1 = x - pipo.WIDTH / 2;
			pipo.x2 = x + pipo.WIDTH / 2;
			pipo.y1 = y - pipo.HEIGHT / 2;
			pipo.y2 = y + pipo.HEIGHT / 2;
			lastMousePosition[0] = x;
			lastMousePosition[1] = y;
			System.out.println("Teleported to: " + x + ":" + y);
		}

	}

	/**
	 * Only returns what is visible (frustum culling), this improves the performance
	 * of the game and the memory used.
	 * 
	 * @return the list of sprites that are visible on the camera.
	 */
	private static ArrayList<Sprite> spritesFrustumCulling(){
		ArrayList<Sprite> sprites = new ArrayList<>();
		for (Sprite s : allSprites){
			if(s.x2 >= 0 && s.x1 <= MainGameLoop.wWidth && s.y2 >= 0 && s.y1 <= MainGameLoop.wHeight){
				sprites.add(s);
			}
		}
		return sprites;
	}

	/**
	 * Initializates everything Creates the player and add sprites to their
	 * arrayLists
	 */
	private static void init(){
		inGame = true;
		MainGameLoop.field.background = null;
		SWindow.getWindow().setResizable(false);
		// MainGameLoop.wWidth/2-Pipo.WIDTH/2, MainGameLoop.wHeight/2-Pipo.HEIGHT/2
		pipo = Pipo.getInstance(MapGenerator.getSpawnpoint(actualLevel)[0], MapGenerator.getSpawnpoint(actualLevel)[1]);
		lastMousePosition[0] = MainGameLoop.field.getMouseX();
		lastMousePosition[1] = MainGameLoop.field.getMouseY();
		addSprites();

		pantsusCounter = 0;
		bleachCounter = 0;

		bleachCounterTxt.changeImage(bleachCounter + "");
		pantsusCounterTxt.changeImage(pantsusCounter + "");

	}

	/**
	 * It gives some time for the game to load
	 * 
	 * @throws InterruptedException
	 */
	private static void loadGameScreen() throws InterruptedException{
		int counter = 0, time = 60;
		int[] lastMousePositionAux = new int[2];
		lastMousePositionAux[0] = lastMousePosition[0];
		lastMousePositionAux[1] = lastMousePosition[1];
		
		Background blackBG = new Background(MainGameLoop.wWidth, MainGameLoop.wHeight, "res/black.png");
		allSprites.add(blackBG);

		Text loadingText = new Text("Loading", "Loading...", MainGameLoop.wWidth / 2 - 150,
				(MainGameLoop.wHeight / 5) * 3 - (126 / 2), 0);
		loadingText.font = MainGameLoop.normalFont;
		loadingText.textColor = MainGameLoop.whiteColor;
		loadingText.unscrollable = true;
		allSprites.add(loadingText);

		Text bleachNeeded = new Text("BleachNeeded", "Bleach needed: " + MapGenerator.bleachNeaded(actualLevel),
				MainGameLoop.wWidth / 2 - 130, (MainGameLoop.wHeight / 5) * 3 - (126 / 4), 0);
		bleachNeeded.font = MainGameLoop.lightFont;
		bleachNeeded.textColor = MainGameLoop.whiteColor;
		bleachNeeded.unscrollable = true;
		allSprites.add(bleachNeeded);
		int puntos = 0;

		String r = "Loading";

		// Bucle loading
		while (true){
			if(counter >= time)
				break;
			else{
				counter++;
				if(counter % 10 == 0){
					puntos++;
					r += ".";
					if(puntos > 3){
						r = "Loading";
						puntos = 0;
					}
					loadingText.changeImage(r);
				}
			}
			MainGameLoop.field.draw(allSprites);
			Thread.sleep(60);
		}
		allSprites.remove(bleachNeeded);
		allSprites.remove(loadingText);
		allSprites.remove(blackBG);
		lastMousePosition = lastMousePositionAux;
	}

	/**
	 * This function gets all the sprites for playing the levels orderly
	 * 
	 * @param level - Level to be played
	 */
	private static void addSprites(){
		// BackGround
		background = new Background(MainGameLoop.wWidth * 2, MainGameLoop.wHeight);
		allSprites.add(background);
		allSprites.addAll(MapGenerator.loadBG(actualLevel));

		sueloGeneral = new Platform("SueloGeneral", 0, MapGenerator.floorHeight, MainGameLoop.wWidth,
				MainGameLoop.wWidth, "");
		allSprites.add(sueloGeneral);

		// Enemies
		allEnemies.addAll(MapGenerator.loadEnemies(actualLevel));
		allSprites.addAll(allEnemies);
		// Platforms
		allPlatforms.addAll(MapGenerator.loadPlatforms(actualLevel));
		allSprites.addAll(allPlatforms);

		// Player
		allSprites.add(pipo);

		// Foreground
		allSprites.addAll(MapGenerator.loadFG(actualLevel));

		// GRID
//		allSprites = MapGenerator.Grids(true, allSprites);

		// Gui
		allSprites.addAll(generateGui());
	}

	/**
	 * Generates the Gui items and their respective texts This is made here and not
	 * in MapGenerator because the GUI will be the same on every level, it won't
	 * change
	 * 
	 * @return - The array of sprites of the GUI
	 */
	private static ArrayList<Sprite> generateGui(){
		ArrayList<Sprite> sprites = new ArrayList<>();
		sprites.add(new GuiSprite("Gui", (MainGameLoop.wWidth / 40) * 27, 18, (int) (guiSpriteSize * 0.9f),
				(int) (guiSpriteSize / 1.15f), "res/GUI/bleach.png"));
		sprites.add(new GuiSprite("Gui", (MainGameLoop.wWidth / 40) * 33, 15, guiSpriteSize, "res/GUI/pantsus.png"));

		bleachCounterTxt = new Text("BleachCounter", bleachCounter + "",
				(MainGameLoop.wWidth / 40) * 29 + (MainGameLoop.wWidth / 40) * 1 / 2, 40, 30);
		bleachCounterTxt.font = MainGameLoop.normalFont;
		bleachCounterTxt.textColor = MainGameLoop.whiteColor;
		bleachCounterTxt.unscrollable = true;
		sprites.add(bleachCounterTxt);

		pantsusCounterTxt = new Text("PantsusCounter", pantsusCounter + "", (MainGameLoop.wWidth / 40) * 36, 40, 30);
		pantsusCounterTxt.font = MainGameLoop.normalFont;
		pantsusCounterTxt.textColor = MainGameLoop.whiteColor;
		pantsusCounterTxt.unscrollable = true;
		sprites.add(pantsusCounterTxt);

		bleachGauge = new GuiSprite("BleachGauge", (MainGameLoop.wWidth / 40) * 1, (MainGameLoop.wHeight / 40) * 1,
				(MainGameLoop.wWidth / 40) * 10 + ((MainGameLoop.wWidth / 40) * 1) / 2, (MainGameLoop.wHeight / 40) * 5,
				"res/BleachGauge/BleachGaugeG.png");
		sprites.add(bleachGauge);

		return sprites;
	}

	/**
	 * Creates the gravity of the character Checks for pressed keys differentiating
	 * when it should run or not Moves the player and then moves every enemy
	 * 
	 * @see PipoLeyenda.SpriteDeriveds.Pipo#checkGround()
	 * @see PipoLeyenda.SpriteDeriveds.Pipo#checkCollisions()
	 */
	private static void inputs(){
		pipo.checkGround();
		pipo.checkCollisions();
		if(SWindow.getWindow().getPressedKeys().contains('d')){
			pipo.moveX = pipo.MOVESPEED;
			pipo.lookingRight = true;
		} else if(SWindow.getWindow().getPressedKeys().contains('a')){
			pipo.moveX = -pipo.MOVESPEED;
			pipo.lookingRight = false;
		} else if(SWindow.getWindow().getPressedKeys().contains('D')){
			pipo.moveX = pipo.MOVESPEED * pipo.RUNSPEED;
			pipo.lookingRight = true;
		} else if(SWindow.getWindow().getPressedKeys().contains('A')){
			pipo.moveX = -pipo.MOVESPEED * pipo.RUNSPEED;
			pipo.lookingRight = false;
		} else
			pipo.moveX = 0;

		if(SWindow.getWindow().getPressedKeys().contains('p') || SWindow.getWindow().getPressedKeys().contains('P')){
			pipo.bleachGauge = 0;
			changeBleachGauge(pipo.bleachGauge);
			shouldStop(actualLevel);
		}

		if(SWindow.getWindow().getPressedKeys().contains('w') || SWindow.getWindow().getPressedKeys().contains('W')){
			pipo.jump();
		}

		pipo.updateMove();

		otherMoves();
	}

	/**
	 * Makes all the enemies and others fall and move
	 */
	private static void otherMoves(){
		for (Enemy e : allEnemies){
			e.move();
			if(e instanceof GravityEnemy){
				GravityEnemy eWalk = (GravityEnemy) e;
				eWalk.gravity();
			}
		}

		for (int i = 0; i < allSprites.size(); i++){
			if(allSprites.get(i) instanceof FallEntity){
				if(allSprites.get(i).delete){
					allSprites.remove(i);
					i--;
					continue;
				}
				FallEntity e = (FallEntity) allSprites.get(i);
				if(!e.canMove && e.firstInstance){
					e.canMove = true;
					e.firstInstance = false;
				}
				e.canMove = shouldStop(actualLevel);
				e.fall();
			}
		}
	}

	/**
	 * Win screen, it has a button to return to main menu Marks the actual level as
	 * completed and unlocks the next
	 * 
	 * @param level - The level that have been played
	 * @throws InterruptedException
	 * @throws IOException 
	 * @see MainGameLoop#levelCompleted(int)
	 */
	private static void endWinning() throws InterruptedException, IOException{
		allSprites
				.add(new Sprite("endZone", 0, 0, MainGameLoop.wWidth, MainGameLoop.wHeight, "res/GUI/endGameZone.png"));
		allSprites.add(new Button("WinButton", MainGameLoop.wWidth / 2 - 128, MainGameLoop.wHeight / 2 + 64,
				MainGameLoop.wWidth / 2 + 128, MainGameLoop.wHeight / 2 + 128));
		pipo.stop();

		// {{ TEXTS
		Text winTextOutline = new Text("Win", "YOU WIN!", MainGameLoop.wWidth / 3 - 10 + 6,
				MainGameLoop.wHeight / 5 + 64 + 3, 15);
		winTextOutline.font = MainGameLoop.boldFont;
		winTextOutline.textColor = MainGameLoop.blackColor;
		winTextOutline.unscrollable = true;
		allSprites.add(winTextOutline);

		Text winText = new Text("Win", "YOU WIN!", MainGameLoop.wWidth / 3 - 10, MainGameLoop.wHeight / 5 + 64, 15);
		winText.font = MainGameLoop.boldFont;
		winText.textColor = MainGameLoop.whiteColor;
		winText.unscrollable = true;
		allSprites.add(winText);

		Text winText2Outline = new Text("Win", "You have all the bleach of the level!",
				MainGameLoop.wWidth / 5 + (MainGameLoop.wWidth / 5) / 2 - 15 + 2, MainGameLoop.wHeight / 4 + 84 + 3,
				15);
		winText2Outline.font = MainGameLoop.lightFont;
		winText2Outline.textColor = MainGameLoop.blackColor;
		winText2Outline.unscrollable = true;
		allSprites.add(winText2Outline);

		Text winText2 = new Text("Win", "You have all the bleach of the level!",
				MainGameLoop.wWidth / 5 + (MainGameLoop.wWidth / 5) / 2 - 15, MainGameLoop.wHeight / 4 + 84, 15);
		winText2.font = MainGameLoop.lightFont;
		winText2.textColor = MainGameLoop.whiteColor;
		winText2.unscrollable = true;
		allSprites.add(winText2);
		// }}

		boolean toChoose = true;
		while (toChoose){
			toChoose = MainGameLoop.checkMouseBool(allSprites);
			MainGameLoop.field.draw(allSprites);
			Thread.sleep(22);
		}
		makeSaveFile();
		MainGameLoop.levelCompleted(actualLevel);
	}

	/**
	 * Saves the actual pantus in a file
	 * @throws IOException 
	 */
	private static void makeSaveFile() throws IOException{
		
		try{
			File file = new File("files/save00.save");
			boolean b = file.createNewFile();
			if(b) {
				BufferedWriter bw = new BufferedWriter(new FileWriter("files/save00.save"));
				bw.write("pantsus:0");
				bw.newLine();
				bw.write("skin:0");
				bw.newLine();
				bw.write("shop:[]");
				bw.flush();
				bw.close();
			}
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			int pants = 0;
			while((line = br.readLine()) != null) {
				if(line.contains("pantsus")) {
					String[] l = line.split(":");
					pants = Integer.parseInt(l[1]);
				}
			}
			
			BufferedWriter bw = new BufferedWriter(new FileWriter("files/save00.save"));
			bw.write("pantsus:"+(pants+pantsusCounter));
			bw.newLine();
			bw.write("skin:"+Pipo.actualSkin);
			bw.newLine();
			bw.write(Shop.getInstance().toString());
			bw.flush();
			
			bw.close();
			br.close();
			
		} catch (Exception e){
			System.out.println("Ha muerto el buffer de files" + e);
		}
	}

	/**
	 * Lose screen to return to main menu or (TODO) repeat the level
	 * 
	 * @param level - The level that have been played
	 * @throws InterruptedException
	 */
	private static void endLoosing() throws InterruptedException{
		// TODO REPEAT LEVEL BUTTON
		allSprites
				.add(new Sprite("endZone", 0, 0, MainGameLoop.wWidth, MainGameLoop.wHeight, "res/GUI/endGameZone.png"));

		allSprites.add(new Button("LoseButton", MainGameLoop.wWidth / 2 - 128, MainGameLoop.wHeight / 2 + 64,
				MainGameLoop.wWidth / 2 + 128, MainGameLoop.wHeight / 2 + 128));
		pipo.stop();

		// {{ TEXTS
		Text winTextOutline = new Text("Win", "YOU LOSE!", MainGameLoop.wWidth / 3 - 50 + 6,
				MainGameLoop.wHeight / 5 + 64 + 3, 15);
		winTextOutline.font = MainGameLoop.boldFont;
		winTextOutline.textColor = MainGameLoop.blackColor;
		winTextOutline.unscrollable = true;
		allSprites.add(winTextOutline);

		Text winText = new Text("Win", "YOU LOSE!", MainGameLoop.wWidth / 3 - 50, MainGameLoop.wHeight / 5 + 64, 15);
		winText.font = MainGameLoop.boldFont;
		winText.textColor = MainGameLoop.whiteColor;
		winText.unscrollable = true;
		allSprites.add(winText);

		Text winText2Outline = new Text("Win", "You lost all your desire for bleach!",
				MainGameLoop.wWidth / 5 + (MainGameLoop.wWidth / 5) / 2 - 15 + 6, MainGameLoop.wHeight / 4 + 84 + 3,
				15);
		winText2Outline.font = MainGameLoop.lightFont;
		winText2Outline.textColor = MainGameLoop.blackColor;
		winText2Outline.unscrollable = true;
		allSprites.add(winText2Outline);

		Text winText2 = new Text("Win", "You lost all your desire for bleach!",
				MainGameLoop.wWidth / 5 + (MainGameLoop.wWidth / 5) / 2 - 15, MainGameLoop.wHeight / 4 + 84, 15);
		winText2.font = MainGameLoop.lightFont;
		winText2.textColor = MainGameLoop.whiteColor;
		winText2.unscrollable = true;
		allSprites.add(winText2);
		// }}

		boolean toChoose = true;
		while (toChoose){
			toChoose = MainGameLoop.checkMouseBool(allSprites);
			MainGameLoop.field.draw(allSprites);
			Thread.sleep(17);
		}
	}

	/**
	 * Checks if player have won (have all bleach of the level) or have lost (it's
	 * bleach desire is 0)
	 * 
	 * @param level - The actual level
	 * @return true if have won, false if gauge is 0
	 */
	private static boolean haveWon(){
		if(pipo.bleachGauge <= 0 || bleachCounter < MapGenerator.bleachNeaded(actualLevel))
			return false;
		else
			return true;
	}

	/**
	 * @param level - The actual player
	 * @return False if pipo has lost all its desire for bleach or has all bleach
	 *         stops the game
	 */
	private static boolean shouldStop(int level){
		if(bleachCounter >= MapGenerator.bleachNeaded(level) || pipo.bleachGauge <= 0)
			return false;
		else
			return true;
	}

	/**
	 * Increases bleach counter. updates the text and increases the bleach gauge
	 * 
	 * @see PipoLeyenda.SpriteDeriveds.Pipo#increaseBleachGauge()
	 */
	public static void increaseBleach(){
		bleachCounter++;
		bleachCounterTxt.changeImage(bleachCounter + "");
		inGame = shouldStop(actualLevel);
		pipo.increaseBleachGauge();
	}

	/**
	 * Increases pantsus counter and updates the text
	 */
	public static void increasePantsus(){
		pantsusCounter++;
		pantsusCounterTxt.changeImage(pantsusCounter + "");
	}

	/**
	 * Updates the bleach gauge graphics
	 * 
	 * @param bleachGaugeNum - Bleach gauge number
	 */
	public static void changeBleachGauge(int bleachGaugeNum){
		inGame = shouldStop(actualLevel);
		bleachGauge.changeImage(bleachGaugeImages[bleachGaugeNum]);
	}

	/**
	 * Cleans all the lists for the next time
	 */
	private static void cleanUP(){
		allPlatforms.clear();
		allSprites.clear();
		allEnemies.clear();
		SWindow.clear();
	}
}
