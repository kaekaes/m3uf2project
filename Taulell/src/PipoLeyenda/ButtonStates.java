package PipoLeyenda;

/**
 * Enum for the different states of buttons
 * 
 * @author Lucas
 */
public enum ButtonStates{
	LOCK, UNLOCK, DONE, COMINGSOON
}